import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParseTest extends BaseTest{

    private static final Logger logger = LoggerFactory.getLogger(ParseTest.class);

    @Test
    @DisplayName("Simple test with one error in log")
    public void simpleTestWithOneErrorInLog() throws InterruptedException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", "[323] ERROR: Invalid input\n\n", "EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        Assertions.assertThat(new File("src/test/resources/oneErrorTest.txt")).hasSameTextualContentAs(tempReportFile);
    }

    @Test
    @DisplayName("Simple test with one extra letter (ZERROR:) in log")
    public void simpleTestWithExtraErrorLetterInLog() throws InterruptedException, IOException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", "[323] ZERROR: Invalid input\n\n", "EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        File empty = File.createTempFile("emptyFile", ".txt");
        Assertions.assertThat(empty).hasSameTextualContentAs(tempReportFile);
        empty.deleteOnExit();
    }

    @Test
    @DisplayName("Simple test with several empty lines and one error in log")
    public void simpleTestWithOneErrorAndEmptyLinesInLog() throws InterruptedException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n","\n\n","\n\n", "[323] ERROR: Invalid input\n\n", "EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        Assertions.assertThat(new File("src/test/resources/oneErrorTest.txt")).hasSameTextualContentAs(tempReportFile);
    }

    @Test
    @DisplayName("Simple test with two errors in log")
    public void simpleTestWithTwoErrorsInLog() throws InterruptedException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", "[123] ERROR: Invalid input\n\n", "[123] ERROR: Something went wrong\n\n","EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        Assertions.assertThat(new File("src/test/resources/twoErrorsTest.txt")).hasSameTextualContentAs(tempReportFile);
    }

    @Test
    @DisplayName("Error inside debug message")
    public void errorInsideDebugInLog() throws InterruptedException, IOException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", "[123] DEBUG: Invalid ERROR: input\n\n","EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        File empty = File.createTempFile("emptyFile", ".txt");
        Assertions.assertThat(empty).hasSameTextualContentAs(tempReportFile);
        empty.deleteOnExit();
    }

    @Test
    @DisplayName("Test with multiple line error in log")
    public void testWithMultipleLineErrorInLog() throws InterruptedException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", """
                [123] ERROR: Invalid input
                Exception in thread "main" java.lang.NullPointerException
                        at com.example.myproject.Book.getTitle(Book.java:16)
                        at com.example.myproject.Author.getBookTitles(Author.java:25)
                        at com.example.myproject.Bootstrap.main(Bootstrap.java:14)
                """, "[123] ERROR: Something went wrong\n\n","EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        Assertions.assertThat(new File("src/test/resources/multipleLineError.txt")).hasSameTextualContentAs(tempReportFile);
    }

    @Test
    @DisplayName("Test with log from Kristina")
    public void testWithLogMessagesFromKristina() throws InterruptedException {
        List<String> messages = Arrays.asList("2019-4-1 13:32:40 [190] User3 logs in\n\n", "2019-4-1 13:33:45 [123] User1 logs in\n\n",
                "2019-4-1 13:33:45 [123] User1 goes to search page\n\n","2019-4-1 13:33:46 [123] User1 types in search text\n\n",
                "2019-4-1 13:33:48 [256] User2 logs in\n\n", "2019-4-1 13:33:49 [190] User3 runs some job\n\n",
                "2019-4-1 13:33:50 [123] User1 clicks search button\n\n", "2019-4-1 13:33:53 [256] User2 does something\n\n",
                "2019-4-1 13:33:54 [123] ERROR: Some exception occured\n\n", "2019-4-1 13:33:56 [256] User2 logs off\n\n",
                "2019-4-1 13:33:57 [190] ERROR: Invalid input\n\n",
                "EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        Assertions.assertThat(new File("src/test/resources/testWithLogFromKristina.txt")).hasSameTextualContentAs(tempReportFile);
    }

    @Test
    @DisplayName("Empty report test")
    public void emptyReportTest() throws InterruptedException, IOException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", "[123] Warning: error input\n\n","EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        File empty = File.createTempFile("emptyFile", ".txt");
        Assertions.assertThat(empty).hasSameTextualContentAs(tempReportFile);
        empty.deleteOnExit();
    }

    @Test
    @DisplayName("Simple test with lowercase error in log")
    public void simpleTestWithLowerCaseErrorInLog() throws InterruptedException, IOException {
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input", "[123] error: Invalid input", "EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        File empty = File.createTempFile("emptyFile", ".txt");
        Assertions.assertThat(empty).hasSameTextualContentAs(tempReportFile);
        empty.deleteOnExit();
    }

    @Test
    @DisplayName("Huge log file test (~100MB) with error message in the end of file")
    public void hugeLogFileTEst() throws InterruptedException, IOException {
        RandomAccessFile hugeLog = new RandomAccessFile(tempLogFile, "rw");
        /*Let's say empty string equals 2 bytes. Then we have to multiple it by 52428800 times.
        We cane make big file with RandomAccessFile setLength() method but it makes one big line instead of multiple newlines
        1GB file check takes approx 30 minutes on my machine cuz of long file creation*/
        logger.debug("Creating huge log file");
        for (long i = 0; i < 52428800L; i++) {
            hugeLog.writeBytes("\r\n");
        }
        hugeLog.close();
        List<String> messages = Arrays.asList("[123] DEBUG: Invalid input\n\n", "[323] ERROR: Invalid input\n\n", "EXIT");
        startTestThreads(messages, tempLogFile, tempReportFile);
        Assertions.assertThat(new File("src/test/resources/oneErrorTest.txt")).hasSameTextualContentAs(tempReportFile);
    }

    @Test
    @DisplayName("Exception thrown by LogReader with wrong file format test")
    void exceptionHandlingTest() {
        IOException thrown = assertThrows(IOException.class, () -> {
            File wrongFile = new File("temp.gfsdg");
            LogReader.readLogAndCreateReport(wrongFile,tempReportFile);
        }, "IOexception expected");
        Assertions.assertThat("Wrong extension").isEqualTo(thrown.getMessage());
    }

}
