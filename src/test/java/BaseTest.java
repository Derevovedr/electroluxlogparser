import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class BaseTest {

    private static final Logger logger = LoggerFactory.getLogger(BaseTest.class);

    File tempLogFile;
    File tempReportFile;

    /**
     * Runnable record для парсинга лога в отдельном потоке
     * @param input - лог файл
     * @param output - файл отчёта
     */
    public record RunnableReaderThread(File input, File output) implements Runnable {

        public void run() {
            try {
                logger.debug("Calling main method readLogAndCreateReport()");
                LogReader.readLogAndCreateReport(input, output);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * Runnable record для записи сообщений в лог в отдельном потоке
     * @param input - лог файл
     * @param messageList - собщения
     */
    public record RunnableWriterThread(List<String> messageList, File input) implements Runnable {

        public void run() {
            if (messageList.size() == 0) {
                return;
            }
            logger.debug("Filling log file with messages");
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(input));
                messageList.forEach(message ->
                {
                    try {
                        bw.write(message);
                        bw.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                bw.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    /**
     * Старт потоков и взаимное ожидание окончания
     */
    void startTestThreads(List<String> messageList, File input, File output) throws InterruptedException {
        logger.debug("Starting threads...");
        Thread reader = new Thread(new RunnableReaderThread(input, output));
        Thread writer = new Thread(new RunnableWriterThread(messageList,input));
        reader.start();
        writer.start();
        reader.join();
        writer.join();
        logger.debug("Threads completed.");
    }

    @BeforeEach
    void init() throws IOException {
        tempLogFile = File.createTempFile("test", ".txt");
        tempReportFile = File.createTempFile("report", ".txt");
    }

    @AfterEach
    void tearDown() {
        if (tempLogFile.exists()) {
            tempLogFile.deleteOnExit();
        }
    }
}
