import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Основной класс, содержащий логику работы.
 * @author Игорь
 * @version 0.1
 */
public class LogReader {

    private static final Logger logger = LoggerFactory.getLogger(LogReader.class);

    /**
     * Метод, который читает лог и пишет отчёт.
     * @param input - лог файл
     * @param output - файл отчёта
     */
    static void readLogAndCreateReport(File input, File output) throws IOException {
        //I assume we are parsing .txt files. But it can be any extension...
        if (!getFileExtension(input).equals(".txt")) {
            throw new IOException("Wrong extension");
        } else {
            BufferedWriter bw = new BufferedWriter(new FileWriter(output));
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(input, "r");
                long pointer;
                while (true) {
                    String currentString = randomAccessFile.readLine();
                    //Check string for regEx matching with pattern ([number] ERROR:)
                    if (currentString != null && currentString.matches(".*\\[\\d+\\]\\sERROR:.*")) {
                        bw.write(currentString);
                        bw.newLine();
                        String nextLine;
                        while ((nextLine = randomAccessFile.readLine()) != null) {
                            currentString = nextLine;
                            if (!nextLine.isEmpty()) {
                                if (!nextLine.equals("EXIT")) {
                                    bw.write(currentString);
                                    bw.newLine();
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                    //Added trigger to exit from writing to file
                    if (currentString != null && currentString.equals("EXIT")) {
                        logger.debug("Get EXIT message, closing reader.");
                        bw.close();
                        break;
                    } else {
                        pointer = randomAccessFile.getFilePointer();
                        randomAccessFile.close();
                        //We can make a delay here on repeating reading the file or implement the watcher pattern
                        //e.g Thread.sleep(1000) will continuously check the file every 1 second
                        randomAccessFile = new RandomAccessFile(input, "r");
                        if (pointer > randomAccessFile.length()) {
                            randomAccessFile.seek(0);
                        } else randomAccessFile.seek(pointer);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            bw.close();
        }
    }

    /**
     * Вспомогательный метод, возвращает расширение файла.
     * @param file - файл, для которого необходимо вернуть расширение
     */
    private static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return name.substring(lastIndexOf);
    }
}
