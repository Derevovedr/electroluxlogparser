# Electrolux Log Parser

Test project for Electolux. This buddy parses log and creates a report with all errors inside.

Continuously reads log file until gets an "EXIT" string (trigger to exit from thread).

Reads log and writes report in different threads.
